using Xunit;
using System;
using ParcelService;
using System.Collections.Generic;
using System.Linq;

namespace ParcelService.Tests
{

    public class ParcelService_Tests
{

private readonly ParcelServiceClass _parcelService;

public ParcelService_Tests()
{
    _parcelService = new ParcelServiceClass();
}


[Theory]
[InlineData(1, 1, 1, ParcelSize.Small)]
[InlineData(9,9,9, ParcelSize.Small)]
[InlineData(10,10,10, ParcelSize.Medium)]
[InlineData(49,49,49, ParcelSize.Medium)]
[InlineData(50,50,50, ParcelSize.Large)]
[InlineData(99,99,99, ParcelSize.Large)]
[InlineData(100,99,99, ParcelSize.XL)]
[InlineData(99,100,99, ParcelSize.XL)]
[InlineData(99,99,100, ParcelSize.XL)]

public void ParcelService_Return_Correct_ParcelSize(int a, int b, int c, ParcelSize expected)
{
    var result = _parcelService.CalculateParcelSize(new Parcel(a,b,c));

    Assert.Equal(result, expected);
}


public static IEnumerable<object[]> ParcelData =>
        new List<object[]>
        {
            new object[] { 1,1,1, 3M },
            new object[] { 10,10,10, 8M },
            new object[] { 50,50,50, 15M},
            new object[] {100,100,100, 25M}
        };
[Theory]
[MemberData(nameof(ParcelData))]
public void ParcelService_Return_Correct_Costs(int a, int b, int c, decimal expected)
{
    var result = _parcelService.ProcessParcels(new Parcel[]{new Parcel(a,b,c)}, false);
    Assert.Equal(result.TotalPrice, expected);
}


    public static IEnumerable<object[]> Data =>
        new List<object[]>
        {
            new object[] { new Parcel[]{new Parcel(1,1,1)}, true, 6M },
            new object[] { new Parcel[]{new Parcel(20,20,20)}, true, 16M },
            new object[] { new Parcel[]{new Parcel(50,50,50)}, true, 30M },
           
        };

[Theory]
[MemberData(nameof(Data))]
public void Speedy_Shipping_Doubles_Order_Cost(Parcel[] parcels, bool speedyShipping, decimal expected)
{
    var result = _parcelService.ProcessParcels(parcels, speedyShipping);
    
    Assert.Equal(expected, result.TotalPrice);
}

public static IEnumerable<object[]> OrderSummary =>

        new List<object[]>
        {
            new object[] { new Parcel[]{new Parcel(1,1,1)}, true},           
        };

[Theory]
[MemberData(nameof(OrderSummary))]
public void Order_Prints_Shipping_Cost_As_Separate_Item(Parcel[] parcels, bool speedyShipping)
{
    var orderSummary = _parcelService.PrintOrderSummary(_parcelService.ProcessParcels(parcels, speedyShipping));
    Assert.True(orderSummary.Contains("Speedy Shipping"));
}

public static IEnumerable<object[]> ShippingTestData =>

        new List<object[]>
        {
            new object[] { new Parcel[]{new Parcel(20,20,20)}, true, 8M}, 
            new object[] { new Parcel[]{new Parcel(20,20,20)}, false, 8M}       
        };


[Theory]
[MemberData(nameof(ShippingTestData))]
public void Speedy_Shipping_Does_Not_Affect_Parcel_Price(Parcel[] parcels, bool speedyShipping, decimal expected)
{
    var result = _parcelService.ProcessParcels(parcels, speedyShipping);
    Assert.Equal(expected, parcels.FirstOrDefault().Price);
}

public static IEnumerable<object[]> HeavyTestData =>

        new List<object[]>
        {
            new object[] {new Parcel(1,1,1,ParcelSize.Small,51), 1M}, 
            new object[] {new Parcel(20,20,20,ParcelSize.Medium,52), 2M},
            new object[] {new Parcel(50,50,50,ParcelSize.Large,53), 3M},
            new object[] {new Parcel(100,100,100,ParcelSize.XL,54), 4M}
        };

[Theory]
[MemberData(nameof(HeavyTestData))]
public void Heavy_Items_Are_Charged_1_Dollar_Per_KG_Over_50KG(Parcel parcel, decimal expected)
{
    var result = _parcelService.CalculateExtraWeightCharges(parcel);
    Assert.Equal(expected, result);
}


}
}
