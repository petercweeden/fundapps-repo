namespace ParcelService
{
    public struct Dimensions
        {
            private readonly int length;
            public int Length { get { return length; } }

            private readonly int width;
            public int Width { get { return width; } }

            private readonly int height;
            public int Height { get { return height; } }

            public Dimensions(int length, int width, int height)
        {
            this.length = length;
            this.width = width;
            this.height = height;
        }

            public int Volume { get { return this.length * this.width * this.height; } }
        }
}