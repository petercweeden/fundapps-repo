namespace ParcelService
{
    public class ItemDto
    {
        public string Type {get; set;}

        public decimal Cost {get; set;}

        public ItemDto(string type, decimal cost)
        {
            this.Type = type;
            this.Cost = cost;
        }

    }
}