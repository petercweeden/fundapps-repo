﻿using Newtonsoft.Json;

namespace ParcelService;
public class ParcelServiceClass : IParcelService
{
    public ParcelServiceClass()
    {

    }
     public ParcelSize CalculateParcelSize(Parcel p)
        {
            var threshold = new [] { p.Dimensions.Length, p.Dimensions.Width, p.Dimensions.Height}.Max();

            if (threshold < 10)
                return ParcelSize.Small;
            if (threshold < 50)
                return ParcelSize.Medium;
            if (threshold < 100)
                return ParcelSize.Large;
            else
                return ParcelSize.XL;
        }

        public OrderSummary ProcessParcels(Parcel[] parcels, bool speedyShipping)
        {
            //create list of items and populate with the parcels when processed
            List<ItemDto> items = new List<ItemDto>();
            decimal totalPrice = 0M;
            decimal subTotal = 0M;

            //loop parcels and get price, size
            foreach(var p in parcels)
            {
                string itemName = p.Size.ToString();
                p.Size = CalculateParcelSize(p);
                p.Price = Parcel.PriceIndex[p.Size.ToString()];
                subTotal = subTotal + p.Price;
                if(p.IsHeavy == true)
                {
                    itemName = $"{itemName} - Heavy";
                }
                items.Add(new ItemDto(itemName, p.Price));
                subTotal = subTotal + CalculateExtraWeightCharges(p);
            }

            //if speedy shipping, calculate total price
            if(speedyShipping)
            {
                totalPrice = subTotal * 2;
                items.Add(new ItemDto("Speedy Shipping", subTotal));
            }
            else
            {
                totalPrice = subTotal; 
            }
            // generate order summary
            var orderSummary = new OrderSummary
            {
                Items = items.ToArray(),
                TotalPrice = totalPrice
            };

            return orderSummary;

        }

        public string PrintOrderSummary(OrderSummary summary)
        {
            // serialise the object to get a printable summary
            return JsonConvert.SerializeObject(summary);
        }

        public string ProcessOrder(Parcel[] parcels, bool speedyShipping)
        {
            //process the parcels, get their sizes and prices
            var orderSummary = ProcessParcels(parcels, speedyShipping);

            //serialise the object
            var summarySerialized = PrintOrderSummary(orderSummary);

            //print to console
            Console.WriteLine(summarySerialized);

            // return the serialised object for use elsewhere
            return summarySerialized;
            
        }

        public decimal CalculateExtraWeightCharges(Parcel parcel)
        {
            decimal weightCharge = 0M;

            if(parcel.Weight > 50M)
            {
                parcel.IsHeavy = true;
                weightCharge = (parcel.Weight - 50) * 1M;
            }
            
            return weightCharge;
        }
}
