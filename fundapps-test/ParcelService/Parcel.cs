namespace ParcelService
{
    public class Parcel
    {
        public Dimensions Dimensions { get; set; }

        public static Dictionary<string, int> PriceIndex { get { return new Dictionary<string, int> { { "Small", 3 }, { "Medium", 8 }, { "Large", 15 }, { "XL", 25 } }; } }

        public ParcelSize Size { get; set; }

        public Parcel(int width, int length, int height)
        {
            this.Dimensions = new Dimensions(length, width, height);
        }

        public Parcel(int width, int length, int height, decimal weight)
        {
            this.Dimensions = new Dimensions(length, width, height);
            this.Weight = weight;
        }

        public Parcel(int width, int length, int height, ParcelSize size, decimal weight)
        {
            this.Dimensions = new Dimensions(length, width, height);
            this.Weight = weight;
            this.Size = size;
        }

        public decimal Price { get; set; }

        public decimal Weight {get; set;}

        public bool IsHeavy {get; set;} = false;
    }

    

}
