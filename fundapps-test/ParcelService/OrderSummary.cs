namespace ParcelService
{
    public class OrderSummary
    {
        public ItemDto[]? Items {get; set;}

        public decimal TotalPrice {get; set;}
    }
}