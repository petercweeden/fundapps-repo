namespace ParcelService
{
    public interface IParcelService
    {
        public ParcelSize CalculateParcelSize(Parcel p);

        public OrderSummary ProcessParcels(Parcel[] parcels, bool speedyShipping);

        public string PrintOrderSummary(OrderSummary summary);

        public decimal CalculateExtraWeightCharges(Parcel parcel);

    }
}